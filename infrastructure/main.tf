
terraform {
  backend "gcs" {
    prefix = "terraform/state"
  }
}


provider "google" {
  version = "~> 2.9"
}

resource "google_storage_bucket" "static_site" {
  name    = "${var.env_name}.${var.domain}"
  project = var.project_id

  force_destroy = true

  website {
    main_page_suffix = "index.html"
    not_found_page   = "index.html"
  }
}

resource "google_storage_bucket_iam_binding" "public_binding" {
  bucket = google_storage_bucket.static_site.id
  role   = "roles/storage.objectViewer"

  members = [
    "allUsers",
  ]
}
