
provider "cloudflare" {
  version = "~> 1.16"
}

provider "google" {
  version = "~> 2.9"
}


locals {
  domain_slug  = replace(var.domain, ".", "-")
  project_name = var.project_name != "" ? var.project_name : local.domain_slug
}

# Setup domain
resource "cloudflare_zone_settings_override" "site" {
  name = var.domain
  settings {
    challenge_ttl            = 2700
    security_level           = "high"
    opportunistic_encryption = "on"
    automatic_https_rewrites = "on"
    minify {
      css  = "on"
      js   = "on"
      html = "on"
    }
    security_header {
      enabled = true
    }
  }
}

# Wildcard subdomain redirect to google buckets for non-prod environments
resource "cloudflare_record" "subdomain" {
  domain = var.domain
  name   = "*"
  value  = "c.storage.googleapis.com"
  type   = "CNAME"
}


data "google_organization" "org" {
  domain = var.domain
}

data "google_billing_account" "acct" {
  display_name = var.billing_name
}

# Sandbox for project
resource "random_id" "id" {
  byte_length = 4
  prefix      = "${local.project_name}-"
}

resource "google_project" "shell_project" {
  name            = local.project_name
  project_id      = random_id.id.hex
  billing_account = data.google_billing_account.acct.id

  org_id = data.google_organization.org.id
  #   auto_create_network = false

  labels = {
    automated = true
  }
}

resource "google_project_service" "project_apis" {
  project = google_project.shell_project.id
  service = "storage-api.googleapis.com"
}

# Terraform remote state bucket for automation
resource "google_storage_bucket" "tf_state" {
  name    = "${local.project_name}-tf-state"
  project = google_project.shell_project.project_id

  versioning {
    enabled = true
  }

  labels = {
    automated = true
  }
}

# Service account for pipeline
resource "google_service_account" "storage_account" {
  account_id   = random_id.id.hex
  display_name = "Pipeline Storage Admin"
  project      = google_project.shell_project.project_id
}

resource "google_service_account_key" "storage_account_key" {
  service_account_id = google_service_account.storage_account.name
  #   pgp_key = var.pgp_pub_key
}

resource "google_project_iam_member" "storage_admin" {
  project = google_project.shell_project.id
  role    = "roles/storage.admin"
  member  = "serviceAccount:${google_service_account.storage_account.email}" # TODO: Add proper member example to TF docs
}

resource "google_project_iam_member" "storage_object_admin" {
  project = google_project.shell_project.id
  role    = "roles/storage.objectAdmin"
  member  = "serviceAccount:${google_service_account.storage_account.email}" # TODO: Add proper member example to TF docs
}

# TODO: Provision secrets by pushing to GL API
