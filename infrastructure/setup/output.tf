

output "domain" {
  value = var.domain
}

locals {
  project = {
    name       = google_project.shell_project.name
    project_id = google_project.shell_project.id
  }
}

output "project" {
  description = "Project container information"
  value       = local.project
}

output "remote_state" {
  description = "Terraform remote state bucket location"
  value       = google_storage_bucket.tf_state
}


locals {
  storage_account = {
    email                 = google_service_account.storage_account.email
    private_key_encrypted = google_service_account_key.storage_account_key.private_key_encrypted
  }
}

output "storage_account" {
  description = "Service account for pipeline storage admin"
  value       = local.storage_account
}
