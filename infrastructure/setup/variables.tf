
variable "domain" {
  description = "Domain to provision"
  type        = string
}

variable "project_name" {
  description = "Project name"
  type        = string
  default     = ""
}

variable "billing_name" {
  description = "Billing account name to attach services"
  type        = string
}

variable "region" {
  description = "Region target"
  type        = string
  default     = "us-central"
}

variable "pgp_pub_key" {
  description = "Encryption key used to protect generated secrets"
  type        = string
  default     = ""
}