# Project Infrastructure Setup

[[_TOC_]]

## Prerequisites

### Secrets required for creation
- Cloudflare Email
- Cloudflare API Key
- Google Cloud Account
<!-- Gitlab API Key -->
<!-- - PGP Key -->

### Required permissions and APIs
#### Google Cloud Permissions
- Resource manager
- Project creator
- Billing viewer
- iam.serviceAccounts.create

### Tools
- [Terraform](https://www.terraform.io)
- [asdf](https://asdf-vm.com/#/)
- Gitlab

### Tooling Setup
```bash
## Instructions for OSX

## Install asdf and Terraform
brew install asdf

## Switch to proper directory
cd [project]/infrastructure/setup

## Match Terraform version
asdf install

## Install gcloud for first time setup and credentialing
brew cask install google-cloud-sdk
```
[*additional instructions and platforms for asdf](https://asdf-vm.com/#/core-manage-asdf)

### Google Cloud Manual Setup
- Billing account must be created (track name of billing account)
- Organization attached to domain and verified is required

## Commands

### 1st time setup

`google auth application-default login`

Set `$GOOGLE_CREDENTIALS` to the path to the application default credentials

```
docker run -it --rm \
-e CLOUDFLARE_EMAIL='' \
-e CLOUDFLARE_TOKEN='' \
-e GOOGLE_CREDENTIALS='' \
-e TF_VAR_domain='' \
-e TF_VAR_billing_name='' \
-v $PWD:/app \
-w /app \
hashicorp/terraform:light init -input=false
```
```
docker run -it --rm \
-e CLOUDFLARE_EMAIL='' \
-e CLOUDFLARE_TOKEN='' \
-e GOOGLE_CREDENTIALS='' \
-e TF_VAR_domain='' \
-e TF_VAR_billing_name='' \
-v $PWD:/app \
-w /app \
hashicorp/terraform:light plan -input=false
```
```
docker run -it --rm \
-e CLOUDFLARE_EMAIL='' \
-e CLOUDFLARE_TOKEN='' \
-e GOOGLE_CREDENTIALS='' \
-e TF_VAR_domain='' \
-e TF_VAR_billing_name='' \
-v $PWD:/app \
-w /app \
hashicorp/terraform:light apply -input=false
```


## Output
Output contains data that will be loaded into Gitlab variables for the rest of the infrastructure to consume

- domain -> $DOMAIN
- project.project_id -> $TF_VAR_project_id
- remote_state -> $BACKEND_CONFIG
- Storage_account.private_key -> Load to $GOOGLE_CREDENTIALS


