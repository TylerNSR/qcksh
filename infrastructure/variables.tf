variable "domain" {
  description = "Domain name of deployed service"
  type        = string
}

variable "env_name" {
  description = "Environment being deployed"
  type        = string
  default     = "www"
}

variable "project_id" {
  description = "Parent project for resources"
  type        = string
}
